% Discrete wavelet transform
% JFM 6/5/21

clear all 
close all

set(0,'defaultaxesfontname','times');
set(0,'defaultaxesfontsize',20);

% Fine grid
L  = 2*pi;
N  = 1024 ;
x  = linspace(0,L,N);
dx0 = x(2) - x(1) ;

% Maximum coarsening level
max_m = 9 ;

% Nonzero values of h[k] and g[k] for Haar basis ;
h = [1, 1]/sqrt(2);
g = [1,-1]/sqrt(2);

% Nonzero values of h[k] and g[k] for Daubechies 4 Basis (Meneveau 1991)
clear h g
h = [1+sqrt(3), 3+sqrt(3), 3-sqrt(3), 1-sqrt(3)]/(4*sqrt(2));
g = [h(4), -h(3), h(2), -h(1)];

% Nonzero values of h[k] and g[k] for Daubechies 8 Basis
clear h g
h = [0.230377813309, 0.714846570553, 0.630880767930, -0.027983769417, ...
    -0.187034811719, 0.030841381836, 0.032883011667, -0.010597401785] ;
for i = 1:length(h)   
    ii = i - 3 ;
    iii = i - 6 ;
	g(i) = (-1)^-ii*h(3-iii) ;
end

lh = length(h) ; %Define wavelet length

% Example signal
f = @(x)  sin(2*pi*x/L) + sin(64*pi*x/L) ;

% Signal coefficients at finest scale
s{1} = zeros(1,length(x)+(lh-2)) ;
s{1}(1:N) = f(x);

%Set up grid for heat map
for m = 1:max_m
    r(m) = 2^(m-1) ;
    dx(m) = dx0 * r(m) ;
    %m_s(m) = 2*pi*dx(m) / L ;
    km(m) = 2*pi/L * 1/r(m) ;
end
%Create meshgrid for the heat map
[X,M] = meshgrid(x,1:max_m) ;   
%[XX,MM] = meshgrid(x,m_s) ;    %Characterstic length scale of dx*r^(m-1). 
[XXX,MMM] = meshgrid(x,km) ;    %Characteristic wavenumber
W_ps = zeros(length(x),max_m) ;
W_ps_norm = zeros(length(x),max_m) ;
W = zeros(length(x),max_m) ;
A = zeros(length(x), max_m) ;

% Signal coarsening (forward transform)
for m=2:max_m
    r = 2^(m-1);
    s{m} = zeros([1,N/r+(lh-2)]);
    w{m} = zeros([1,N/r+(lh-2)]);
    xx{m} = linspace(0,L,N/r) ;
    NN(m) = N/r ;
    for i=1:N/r
        % Convolute
        % Daubechies 4 has 4-point compact support in physical space
        for j=1:lh
            % From Meneveau (1991)
            % s(m+1)[i] = sum_j h[j-2*i] * s(m)[j]
            s{m}(i) = s{m}(i) + h(j) * s{m-1}(j + 2*(i-1));
            % w(m+1)[i] = sum_j g[j-2*i] * s(m)[j]
            w{m}(i) = w{m}(i) + g(j) * s{m-1}(j + 2*(i-1));
        end
    end
    %Energy at each scale (spatially averaged)
    E(m) = mean(w{m}(:).^2)  ;
    
    %Interpolate the wavelet coefficients for the heat map
    vq{m} = interp1(xx{m},w{m}(1:end-(lh-2)),x) ;
    %A(1:length(w{m}),m) = w{m} ;
    W(:,m-1) = vq{m} ;
    
    vq_ps{m} = abs(vq{m}).^2 ; %power spectrum of interpolated wavelet coefficients
    W_ps(:,m-1) = vq_ps{m}  ;
end
W_ps_norm =  W_ps ;         %Normalize the power spectrum
W_log = log10(W_ps_norm) ;
E = E * dx0 / (2*pi*log(2)) ;%Normalization of power-spectral density (Meneveau 1991)

% tol = 10^-4 ;
% r = find(W_log < tol) ;
% W_log(r) = NaN ;

tol = 10^-6 ;
for m = 2:max_m
    for i = 1:length(x) 
        if W_log(i,m) < tol
            W_log(i,m) = NaN ;
        end
    end
end

% Signal reconstruction (inverse transform)
% for m=max_m-1:-1:1
%     r = 2^(m-1);
%     s_R{m} = zeros([1,N/r]);
%     % Expand s{m+1} and w{m+1}, inserting zeros
%     s_exp  = zeros([1,N/r+(lh-(lh/2)+0)]);
%     w_exp  = zeros([1,N/r+(lh-(lh/2)+0)]);
%     s_exp(1:2:end) = s{m+1};
%     w_exp(1:2:end) = w{m+1};
%     for i=1:N/r
%         for j=1:length(h)
%             s_R{m}(i) = s_R{m}(i) + ...
%                 h(j) * s_exp(j + (i-1)) + ...
%                 g(j) * w_exp(j + (i-1)) ;
%         end
%     end
% end

 %%Plots
% close all
% %Plot coarsened signal
% figure(1); hold on
% legStr = [];
% for m=1:max_m
%     r = 2^(m-1);
%     plot(x(1:r:end),s{m})
%     legStr = [legStr;['m=',num2str(m)]];
% end
% xlabel('x');
% ylabel('s^{(m)}');
% legend(legStr);

% Plot wavelet coefficients
% figure(2); hold on
% legStr = [];
% for m=2:max_m
%     r = 2^(m-1);
%     plot(x(1:r:end),w{m}(1:end-(lh-2)))
%     legStr = [legStr;['m=',num2str(m)]];
% end
% xlabel('x');
% ylabel('w_m');
% legend(legStr);

% %Plot signal reconstruction
% figure(3)
% plot(x,s{1},'k','LineWidth',2); hold on
% legStr = [];
% for m=max_m-1:-1:1
%     r = 2^(m-1);
%     plot(x(1:r:end),s_R{m})
%     legStr = [legStr;['S_r, m=',num2str(m)]];
% end
% xlabel('x');
% ylabel('f(x)');
% legend(['f(x)    ';legStr]);
% 
% 
% figure(4) ; hold on
% legStr = [] ;
% for m = 2:max_m ;
%     r = 2^(m-1) ;
%     plot(x(1:end),vq_ps{m})
%     %legStr = [legStr;['m=',num2str(m)]];
% end
% xlabel('x')
% ylabel('W_m')
% legend(legStr)


figure(5) ; hold on
contour(XXX,MMM,W_log')
% m_N = 10 ;
% contour(X(1:(max_m-1) - (max_m - m_N),:),M(1:(max_m-1) - (max_m - m_N),:),W_ps_norm(:,1:(max_m-1) - (max_m - m_N))')
colorbar
set(gca, 'YScale', 'log')
% set(gca,'ColorScale','log')
xlabel('x')
ylabel('k_m')
% axis([0,3,0,6])

% figure(7) ; hold on
% contour(XXX,MMM,W')
% colorbar
% set(gca,'Yscale','log')
% xlabel('x')
% ylabel('k_m')

% figure(6) ; hold on
% plot(km,E) 
% xlabel('k_m')
% ylabel('E(k_m)')
